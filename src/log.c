#include "nfq.h"

#if 0
extern char* timestamp(void) 
{
    time_t curtime;
    struct tm *loctime;
    char *buf;
    char *ptr;
                       
    /* Get the current time. */
    curtime = time (NULL);
                              
    /* Convert it to local time representation. */
    loctime = localtime (&curtime);
                                     
    /* Print out the date and time in the standard format. */
    buf = asctime (loctime);
    ptr = strrchr(buf,'\n');
    *ptr='\0'; // chomp
    return buf;
}
#endif

extern char* timestamp(void)
{
    time_t rawtime;
    struct tm* timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    static char _retval[20];
    strftime(_retval, sizeof(_retval), "%Y-%m-%d %H:%M:%S", timeinfo);

    return _retval;
}

extern void wout(int level, char *str, ...)
{
    va_list arglist;
    char lbuf[LOGBUF_SIZE];
    int trunc=0;
    FILE *fd;
    char *slevel;

    if (cfg->log_level < level) return; 
    
    switch (level)
    {
        case LOG_DEBUG:    
            fd = stdout;
            slevel = "DEBUG";
            break;
        case LOG_INFO: 
            fd = stdout;
            slevel = "INFO";
            break;
        case LOG_NOTICE: 
            fd = stdout;
            slevel = "NOTICE";
            break;
        case LOG_WARNING: 
            slevel = "WARNING";
            fd = stderr;
            break;
        case LOG_ERR:  
            slevel = "ERROR";
            fd = stderr;
            break;
        default:
            if (cfg->syslog) {
                syslog(LOG_ERR,"%s: Invalid log level (%d) - bug in %s\n",__func__,level,pgm);
            } else {
                fprintf(stderr,"%s: Invalid log level %d - bug in %s\n",__func__,level,pgm);
            }
            exit (EXIT_FAILURE);
    }

    va_start(arglist,str);
    memset(lbuf,0,sizeof(lbuf));
    trunc = vsnprintf(lbuf,sizeof(lbuf)-1,str,arglist);
    if (trunc > sizeof(lbuf)-1)
    {
        if (cfg->syslog) {
            syslog(LOG_ERR,"%s: logline too long for buffer! The next log "
                    "line will be truncated to %zu - (as a hotfix: adjust "
                    "LOGBUF_SIZE and recompile!)\n",
                    __func__,sizeof(lbuf)-1);
        } else {
            fprintf(stderr,"[%s][%d][%s][ERROR] %s: logline too long for "
                    "buffer - next log line will be truncated to %zu - "
                    "(fix: adjust LOGBUF_SIZE and recompile!)\n", 
                    pgm,pid,timestamp(),__func__,sizeof(lbuf)-1);
        }
    }

    va_end(arglist);
    if (cfg->syslog) {
        syslog(level,"queue=%d: %s",cfg->queue_num, lbuf);
    } else {

        fprintf(fd,"[%s][%s][%d][qnum %d][%s]: %s\n", 
                timestamp(), pgm, pid, cfg->queue_num, slevel, lbuf);
        fflush(fd);
    }
    return;
}
