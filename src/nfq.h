#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <time.h>
#include <syslog.h>
#include <errno.h>
#include <ctype.h>
#include <limits.h>
#include <signal.h>

#include <bsd/bsd.h> /* strlcpy/strlcat (-lbsd) */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <arpa/inet.h>

#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/ip6.h>

#include <linux/netfilter.h>
#include <libnetfilter_queue/libnetfilter_queue.h>

// required for packet mangling
#include <libnetfilter_queue/libnetfilter_queue_udp.h>
#include <libnetfilter_queue/libnetfilter_queue_ipv4.h>

#include <cap-ng.h>


#ifndef VERSION
#define VERSION "x.x"
#endif

#ifndef BUILD_DATE
#define BUILD_DATE "unknown"
#endif

/*          GLOBALS             */
#define CONFIG_DEFAULT_IPV4_INJECT "0.0.0.0"
#define CONFIG_DEFAULT_IPV6_INJECT "::"
#define CONFIG_DEFAULT_WHITELIST_PATH "/etc/nfq/whitelist"
#define PACKET_BUFSIZE 0xffff /* 65535 */
#define DOCUMENTATION_URL "https://gitlab.com/jbauernberger/nfq"
#define PROC_NFNL_QUEUE "/proc/net/netfilter/nfnetlink_queue"

/* logging */
#define LDEBUG(...) wout(LOG_DEBUG,__VA_ARGS__)
#define LINFO(...) wout(LOG_INFO,__VA_ARGS__)
#define LNOTICE(...) wout(LOG_NOTICE,__VA_ARGS__)
#define LWARN(...) wout(LOG_WARNING,__VA_ARGS__)
#define LERR(...) wout(LOG_ERR,__VA_ARGS__)

// log lines will be truncated to this if log message exceeds the value
#define LOGBUF_SIZE 1024

struct _opt_cfg {

    uint16_t queue_num;
    uint8_t log_level;
    uint8_t syslog;
    uint8_t syslog_facility;
    uint8_t dryrun;
    uint8_t rewrite_answer;
    uint8_t rewrite_A;
    uint8_t rewrite_AAAA;

    int8_t renice;          /* -20 -> +19 */
    uint16_t port;          

    unsigned char rewrite_ip_a[sizeof(struct in6_addr)];
    char rewrite_ip_a_str[16];
    unsigned char rewrite_ip_aaaa[sizeof(struct in6_addr)];
    char rewrite_ip_aaaa_str[40];

    char whitelist_path[PATH_MAX];
};

typedef struct _opt_cfg opt_cfg;

/*
 * Data Type Declarations
 */
struct _list_t {
       char *str;
       struct _list_t * next;
};

typedef struct _list_t list_t;

struct _query_list {
    uint16_t n_queries;
    char **urls;
}; 

typedef struct _query_list query_list;

struct __attribute__((__packed__)) _dnshdr {
    u_int16_t t_id;
    u_int16_t flags;
    u_int16_t n_questions;
    u_int16_t n_answer_rrs;
    u_int16_t n_authority_rrs;
    u_int16_t n_additional_rrs;
};

typedef struct _dnshdr dnshdr;

struct __attribute__((__packed__)) _q_ftr 
{
    u_int16_t q_type;
    u_int16_t q_class;
};
typedef struct _q_ftr q_ftr;

struct __attribute__((__packed__)) _dns_answer
{
    uint16_t offset;   //TO-DO only support fully compressed name format
    uint16_t type;
    uint16_t class;
    uint32_t ttl;
    uint16_t len;
};
typedef struct _dns_answer dns_answer;

extern pid_t pid;
extern char *pgm;
extern opt_cfg *cfg;

/* 
 * Function Declarations 
 */
__BEGIN_DECLS
size_t xstrlcpy(char *dst, const char *src, size_t size);
char * xstrdup(char *s);
void * xrealloc(void *p, size_t n);
void * xmalloc(size_t n);
void xfree(void *p);
void *opt_cfg_malloc(void);
void opt_cfg_free(opt_cfg *c);
list_t * list_t_malloc(void);       
void list_t_free(list_t *l);
query_list extract_urls(unsigned char *payload);
void free_urls(query_list qlist);
uint32_t print_pkt (FILE *fp, struct nfq_data *tb);
void wout(int level, char *str, ...) __attribute__((format(printf,2,3)));char* timestamp(void);
unsigned char * mangle_packet(unsigned char *payload, int pkt_sz);
__END_DECLS
