#!/bin/bash

## if needed change these options below:
NFQ=/usr/local/sbin/nfq
LOG=/var/log
# Please add all your usual options to start nfq
# DO NOT add the "--queue-num" option since we pass them hardoded below!!
#
OPTS="--syslog --log-level notice --facility LOG_DAEMON --renice -10 --rewrite-answer"

###############################################################################

ARGS=$*

if [ "$EUID" -ne 0 ]; then 
    echo "$0 needs to be run as root"
    #exit 1
fi

if [ "$ARGS" == "" ]; then
    echo "$0 requires range argument in the format queue_start:queue_end (e.g.: 0:3)"
    exit 1
fi

NAME=`basename ${NFQ}`

if [ `whoami` != "root" ]; then
    echo "$NAME needs root"
    exit 1
fi

QUEUE_START=$(echo $ARGS | cut -f1 -d ':')
QUEUE_END=$(echo $ARGS | cut -f2 -d ':')

if [[ -z "$QUEUE_START" || -z "$QUEUE_END" ]]; then
    echo "$0: incorrect command line arg. Queue range information not understood."
    exit 1
fi

# are we piping output to log-files manually?
USE_SYSLOG=
if [[ "$OPTS"  =~ .*"--syslog".* ]]; then 
    USE_SYSLOG=1
else
    echo "Not starting $NAME with --syslog option. You can check for logs in $LOG"
fi


while [ $QUEUE_START -le $QUEUE_END ]
do
    if [ -n "$USE_SYSLOG" ]; then
        # we don't expect output. log messages are directly sent to syslog
        LOGFILE=/dev/null
    else
        LOGFILE=${LOG}/${NAME}-queue-${QUEUE_START}.log

        ## Sylog is not used. We need to manage logs here:

        ## move existing logs to prevent them from getting overwritten:
        if [ -e ${LOGFILE} ]; then
            BACKUP_DATE=$(date +%Y-%m-%d_%H:%M:%S)
            BACKUP_LOG="`echo $LOGFILE| sed -re 's/\.log//g'`-${BACKUP_DATE}.log"
            echo "Moving existing ${LOGFILE} to ${BACKUP_LOG}"
            mv ${LOGFILE} ${BACKUP_LOG}
        fi

    fi

    echo "Starting ${NAME} to listen on queue number ${QUEUE_START}:"
    echo "${NFQ} ${OPTS} --queue-num ${QUEUE_START} &> ${LOGFILE} &"
    ${NFQ} ${OPTS} --queue-num ${QUEUE_START} &> ${LOGFILE} &
    ((QUEUE_START++))
done

ex=$?
if [ $ex -ne 0 ]; then
    echo "starting nfq seems to have failed"
fi

exit $ex
